| PROJECT | EDITORS | STATUS |
| ------ | ------ | ------ |
| Marcdown to HTML Converter | Biechl M. | **finished** |


### 1. Marcdown to HTML Converter ###

#### Aspects ####
*  .md file input
*  result should be a .html file
*  maybe with bulk convert

--- 

### Editors of this Repository ###
 * Biechl Matthias