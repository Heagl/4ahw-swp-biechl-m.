package markdown_to_html_convertor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.*;

public class Main {

	public static void main(String[] args) {
		boolean help = true;
		String content = "" ;
		String collumn = " ";
		String result = " ";
		String result1 = " ";
		boolean edited = false;
		boolean ulStart = true;
		try {
			FileReader fileIn = new FileReader(args[0]); 
			BufferedReader in = new BufferedReader(fileIn);

			FileWriter fileExit = new FileWriter(args[0]+".html");
			BufferedWriter exit = new BufferedWriter(fileExit);
			while(help)
			{
				edited = false;
				collumn = in.readLine();
				//System.out.println(collumn);
				if(collumn != null)
				{

					while(collumn.contains("#"))
					{
						collumn = collumn.replaceFirst("######", "<h6>");
						collumn = collumn.replaceFirst("######", "</h6>");
						collumn = collumn.replaceFirst("#####", "<h5>");
						collumn = collumn.replaceFirst("#####", "</h5>");
						collumn = collumn.replaceFirst("####", "<h4>");
						collumn = collumn.replaceFirst("####", "</h4>");
						collumn = collumn.replaceFirst("###", "<h3>");
						collumn = collumn.replaceFirst("###", "</h3>");
						collumn = collumn.replaceFirst("##", "<h2>");
						collumn = collumn.replaceFirst("##", "</h2>");
						collumn = collumn.replaceFirst("#", "<h1>");
						collumn = collumn.replaceFirst("#", "</h1>");
						if(!collumn.contains("#"))
						{
							result += collumn;
						}
					}

					if((collumn.contains("---") && (!collumn.contains("------"))))
					{
						result += collumn.replaceFirst("---", "<hr>");
					}


					while(collumn.contains("*"))
					{
						collumn = collumn.replaceFirst("[*][*][*]", " <b><i>");
						collumn = collumn.replaceFirst("[*][*][*]", "</b></i> ");
						collumn = collumn.replaceFirst("[*][*]", " <b>");
						collumn = collumn.replaceFirst("[*][*]", "</b> ");
						collumn = collumn.replaceFirst("[*]", " <i>");
						collumn = collumn.replaceFirst("[*]", "</i> ");
						if(!collumn.contains("*"))
						{
							result += collumn;
						}
					}

					if((collumn.contains("- "))&& (!collumn.contains("--")))
					{

						result += "<ul>";
						result += collumn.replaceFirst("-", "<li>");
						result += "</li>"+"</ul>";
					}

				}
				else{
					help = false;
				}
			}
			System.out.println(result);
			//System.out.print(content);
			result.replaceAll("�", "&auml");
			result.replaceAll("�", "&Auml");
			result.replaceAll("�", "&ouml");
			result.replaceAll("�", "&Ouml");
			result.replaceAll("�", "&uuml");
			result.replaceAll("�", "&Uuml");
			result.replaceAll("�", "&euro");
			exit.write("<html><head></head><body>");
			exit.write(result);
			exit.write("</body></html>");
			in.close();
			fileIn.close();
			exit.close();
			fileExit.close();

		}
		catch(IOException i) {
			System.out.println(i.getMessage());
		}
	}

}