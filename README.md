| PROJECT | EDITORS | STATUS |
| ------ | ------ | ------ |
| Marcdown to HTML Converter | Biechl M. | **finished** |
| Sortingalgorithms | Biechl M. | **finished** |
| Linked List with Insertion Sort | Biechl M. | *working partly* |
| Differend recursive Methods (PD,palindrom,binarySearch) | Biechl M. |**finished** |


### 1. Marcdown to HTML Converter ###

#### Aspects ####
-  .md file input
-  result should be a .html file
-  maybe with bulk convert

--- 

### 2. Sortieralgorithmus ###

---

### 3. Linked List with Insertion Sort ###

#### Aspects ####
- singel linked List that can add numbers at every Position

---

### 4. Differend recursive Methods (PD,palindrom,binarySearch) ###

#### Aspects ####
- gets the number at a specific Line and Position on the pascalian Triangle
- checks if a word is a palindrom or it isnt
- searches the Value with recursive methods in an Array

---


### Editors of this Repository ###
- Biechl Matthias