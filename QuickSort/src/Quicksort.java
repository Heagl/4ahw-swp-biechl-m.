import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Quicksort {


	ArrayList<Integer> r = new ArrayList();

	public Quicksort() {

	}

	/*public ArrayList QuicksortArray(ArrayList<Integer> array){
		this.r = array;

		return sortArray(r);
	}*/

	public ArrayList sortArray(ArrayList<Integer> array)
	{
		return sortArrayHelper(array, 0, array.size()-1);
	}

	private ArrayList sortArrayHelper(ArrayList<Integer> r, int lowest, int highest){
		int indx = arrayHalved(r, 0, highest);

		if((lowest < indx-1) && (indx - 1 >0))
		{
			sortArrayHelper(this.r, lowest, indx);
		}
		if((highest > indx) && (indx - 1 >0))
		{
			sortArrayHelper(this.r, indx, highest);
		}

		return this.r;

	}

	private int arrayHalved(ArrayList<Integer> r, int low, int high){
		int pivot = r.get(low); 

		while(low <= high)
		{
			while(r.get(low) < pivot)
			{
				low++;
			}
			while(r.get(high) > pivot)
			{
				high--;
			}

			if(low <= high)
			{
				swapElements(this.r, low, high);
				low++;
				high--;
			}
		}

		return low;
	}

	private void swapElements(ArrayList r, int i, int j){
		Collections.swap(r, i, j);
	}
}

