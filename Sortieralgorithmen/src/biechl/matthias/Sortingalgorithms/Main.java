package biechl.matthias.Sortingalgorithms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class Main {
	static long starttime;
	static int number;
	static int z = 10;
	static int[] array2 = new int[z];
	static ArrayList<Integer> r = new ArrayList<Integer>();
	static Random randomgen = new Random();
	public static void main(String[] args)
	{
		for(int i = 0; i<z; i++)
		{
			fill(i);
		}
		//System.out.println(r);
		sort();
		System.out.println(r);
		System.out.println(System.currentTimeMillis()-starttime);
		
		starttime = System.currentTimeMillis();
		Arrays.sort(array2);
		System.out.println(System.currentTimeMillis()-starttime);
		
	}
	
	public static void fill(int i)
	{
		r.add(randomgen.nextInt(z*10));
		array2[i] = r.get(i);
	}
	
    public static void sort() {
    	starttime = System.currentTimeMillis(); 
    	number = r.size();
        sortArray(0, number - 1);
        
    }

    private static void sortArray(int low, int high) {
        int i = low, j = high;

        int lengthHalved = r.get(low + (high-low)/2);

        while (i <= j) {
            while (r.get(i) < lengthHalved) {
                i++;
            }

            while (r.get(j) > lengthHalved) {
                j--;
            }

            if (i <= j) {
                swapEntries(i, j);
                i++;
                j--;
            }
        }
        if (low < j)
            sortArray(low, j);
        if (i < high)
            sortArray(i, high);
    }

    private static void swapEntries(int i, int j) {
        Collections.swap(r, i, j);
    }
}
