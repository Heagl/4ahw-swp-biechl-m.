
public class binarySearch {

	static int[]array= new int[10];

	public static String binarySearch(int nbr, int[] arrayInt)
	{
		for(int i = 0; i< arrayInt.length; i++)
		{
			array[i] = arrayInt[i];
		}

		return "Das Element befindet sich auf der Stelle " +binarySearchHelp(nbr, 0, array.length-1)+". ";
	}

	private static int binarySearchHelp(int nbr, int min, int max)
	{
		if(max < min)
			return -1;
		int i = (min + max)/2;
		if(array[i] == nbr)
			return i;
		else if(array[i] > nbr)
			return binarySearchHelp(nbr, min, i-1);
		else
			return binarySearchHelp(nbr, i+1 , max);
	}
}
