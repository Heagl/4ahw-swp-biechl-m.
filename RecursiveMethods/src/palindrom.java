
public class palindrom {

	
	
	public static boolean isPalindrom(String pal)
	{
		pal = pal.toUpperCase();
		
		if((pal.length() == 0) || (pal.length() == 1))
		{
			return true;
		}
		
		if(pal.charAt(0) == pal.charAt(pal.length()-1))
			return isPalindrom(pal.substring(1,pal.length()-1));
		return false;
	}
}



