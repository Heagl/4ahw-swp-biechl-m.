
public class main {

	static int size = 9;
	static int[] arrayInt = new int[size];
	public static void main(String[] args) {
		
		for(int i = 0; i<size; i++)
			arrayInt[i] = i;
		
		palindrom pal = null;
		
		int i = 5;
		int k = 2;
		
		recursivesPaskalschesDreieck pd = null;
		System.out.println(pd.rec(i, k));
		
		System.out.println(pal.isPalindrom("anna"));
		System.out.println(pal.isPalindrom("Regal"));
		System.out.println(pal.isPalindrom("Lagerregal"));
		System.out.println(pal.isPalindrom("Otto"));

		System.out.println(facRec(9));
		System.out.println(facRecEnd(9));
		
		System.out.println(binarySearch.binarySearch(3, arrayInt));
		
	}
	
	public static int fac(int n)
	{
		int ret = n;
		for(int i = 1; i<=n; i++)
			ret = ret*i;
		return ret;
	}
	
	
	
	public static int facRec(int n)
	{
		if(n == 1)
			return 1;
		
		return n * facRec(n-1);
	}
	
	public static int facRecEnd(int n){
		return facRecEndHelp(n, 1);
	}
	
	private static int facRecEndHelp(int n, int m){
		if(n == 0) 
			return m;
		return facRecEndHelp(n-1, n*m);
	}

}
