
public class recursivesPaskalschesDreieck {
	
	public static int rec(int i, int k)
	{
		if((k == 0) || (i == k))
		{
			return 1;
		}

		return (rec(i-1,k-1)) + rec(i-1,k);
	}
	
	/*public static int recEnd(int i, int k)
	{
		return recEndHelp(i, k, 1 , 1);
	}
	
	private static int recEndHelp(int i, int k, int sum1, int sum2)
	{
		if((k == 0) || (i == k))
		{
			return sum1+sum2;
		}

		return (rec(i-1,k-1, )) + rec(i-1,k, );
	}*/
}
